![Carried by the Wind](banner.jpg)

# [R.I.P.](RalfWilliFiedler.md) dear [Ralf](http://Pannewitz.com/ma/Ralf/)
and
# Thank You for Your music !!!
![Carried by the Wind](avatar-large.jpg)
# Carried by the Wind

- [@ Amazon.de](https://www.amazon.de/s?k=%22The+Soulmates%22+%22Carried+by+the+Wind%22)
- [@ SoundCloud.com](https://SoundCloud.com/soulmates-music/)
  - [tracks](https://soundcloud.com/soulmates-music/tracks)

## Lyrics

| Song                      | Time     |
| :------------------------ | --------:|
| [Bye, Bye, Blue Lagoon](ByeByeBlueLagoon.html)	| 3:27 min |
| Let’s Follow the Sun      | 3:29 min |
| Better be Off             | 3:51 min |
| Spick and Span            | 2:57 min |
| II’m in Malta             | 3:18 min |
| Salt Melts Ice            | 3:06 min |
| Lost in Oblivion          | 3:05 min |
| Shiva                     | 4:35 min |
| [Another Groundhog Day](AnotherGroundhogDay.html)	| 3:58 min |
| Born to Give Love         | 4:02 min |
| The Secret of Your Heart  | 4:55 min |
| You shed some tears       | 4:02 min |
| Fly my little bird        | 3:10 min |
| [In Every Heart](InEveryHeart.html)			| 6:22 min |
| No Way Back               | 3:46 min |

----
## Mit wem?

* Olaf Senkbeil (vocal and keyboard)
* Mike Glenn (vocal and percussions)
* Vladimir Ney (piano and keyboards)
* Gottfried Koch (baseguitar and guitar)
* Fjol van Forbach (lead guitar)
* Boris Recke (drums)
* Andrej Rein (accordeon, akustic guitar)
* Roman Kazak (pan-pipe)

---
![Carried by the Wind](avatar-large.jpg)
---
