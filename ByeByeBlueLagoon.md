![Carried by the Wind](banner.jpg)

# Bye, Bye, Blue Lagoon

```
Blue clear sky above us as we climbed up the hill,
had a wonderful view of St. Pauls and Paceville
fishing boats returned in the late afternoon,
bye, bye good bye, blue lagoon
```

```
Prehistorical temples and ancient knights
we enjoyed our dinner by candle lights
swum in the water 'till the fingers got prune
bye, bye good bye, blue lagoon
```

```
we visited cathedrals, palaces and forts,
spent other days at some water resorts
we enjoyed scuba diving ­ mostly at noon,
bye, bye good bye, blue lagoon
```

```
The jewel in the south of the Mediterranean sea
we had to leave and to return we agreed
it will not be long we'll come very soon
bye, bye good bye, blue lagoon
```

```
It was a summer of love, a summer of dreams,
tears of farewell down your cheeks ran like streams
our last common night under full moon
bye, bye good bye, blue lagoon
our last common night under full moon
bye, bye good bye, blue lagoon
```
----
received on 2015-10-29

---
![Carried by the Wind](avatar-large.jpg)
---
