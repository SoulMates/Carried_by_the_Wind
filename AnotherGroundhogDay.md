![Carried by the Wind](banner.jpg)

# Another Groundhog Day

```
It’s Monday morning I get out of my bed
open the window to clear my head
It feels like always,
like another groundhog day
```

```
Time to start taking the bus
watching people following us
they look like always,
like another groundhog day
```

```
Feel your tears on my shoulder
feel the pain in my heart,
I got wiser, I’ve got older
and take my tears –
for an other groundhog day
```

```
Leaving the bus, taking the train,
suddenly it’s raining again
it sounds like always,
like another groundhog day
```

```
Lot’s of people are talking today
some are eating and drinking the way,
it smells like always,
like another groundhog day
```

```
Feel your tears on my shoulder
feel the pain in my heart,
I got wiser, I’ve got older
and take my tears –
for an other groundhog day
```

```
I don’t care going be late again,
do my job the best way I can,
it works like always,
like another groundhog day
```

```
I go home it’s already late,
directly home, I don’t have a date
it hurts like always,
like another groundhog day
```

```
Feel your tears on my shoulder
feel the pain in my heart,
I got wiser, I’ve got older
and take my tears – 
for an other groundhog day
```

```
Feel your tears on my shoulder
feel the pain in my heart,
I got wiser, I’ve got older
and take my tears –
for an other groundhog day
for an other groundhog day
```
---
![Carried by the Wind](avatar-large.jpg)
---
