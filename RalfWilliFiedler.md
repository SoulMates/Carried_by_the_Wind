# Ralf Willi Fiedler

```
	* 31. Juli 1955 in Hamburg,
	+ 26. Nov. 2020 in Braak.
```

- (2010-10-11_Mümi.pdf)[2010-10-11_Mümi.pdf]

----
## 26. November 2020

> At 13:58 26.11.2020, Marc <MerkerMachine@gmx.de> wrote:

> > Lieber Andreas, Es tut mir leid dir mitzuteilen, dass Ralf heute Nacht verstorben ist.
> >  Er ist friedlich und ohne Schmerzen eingeschlafen und ist jetzt bei den Engeln.
> >  Ich habe ihn dabei begleitet und es ist trotzdem sehr schwer jetzt Abschied zu nehmen.
> >  Ich wünsche uns viel Kraft und wir werden Ralf immer in unseren Herzen tragen.
> >  Lass uns gerne die Tage telefonieren.
> >
> >  Lieben Gruß, Marc

> Vor 10 Jahren und drei Wochen war Peter kurz in Europa, und bei mir - und wir lernten Ralf kennen.
>
> Auf 'Zuruf' von Mümi. Und Ralf nahm 'Zuflucht' bei mir in Braak - unsere 'Anstalt' wurde begründet.
>
> [http://Pannewitz.com/ma/Ralf/](http://pannewitz.com/ma/Ralf/target0.html)
>
> Vor 5 Jahren, im Sommer 2015 nahm er Win Win Maw und mich mit nach Timmendorf und nach Lübeck und und und.
>
> [http://Pannewitz.com/ma/Ralf/](http://pannewitz.com/ma/Ralf/)
>
>
> Wir erinnern einen ganz ganz besonderen Mann.
> Ich verliere einen sehr guten Freund.
>
> Wir durften -körperlich fern und geistig nah- miterleben wie toll sein geliebter Sohn und seine ganze Familie bei ihm stand.
>
> Lasst uns seiner Familie Kraft senden und ihre Trauer mit Ihnen teilen.
>
> R.I.P. Ralf
> Andreas & Win Win Maw

----

### Kalle Soll - Kalle.Soll@T-online.de

> Schönen guten Tag lieber Andreas,
>
> danke für die leider sehr traurige Nachricht. Nachdem ihr euch im Sommer noch einmal getroffen habt, waren Bärbel und ich bei Ralf.
>
> Er hat  für uns eine wunderbare Rahmung von einer Arbeit von Bärbels Opa vorgenommen (s. Anhang - noch ohne Rahmen)) - hängt jetzt bei mir im Flur.
>
> Aus dem "mir" kannst du erkennen, dass ich alleine lebe. Bärbel ist leider am 28.5.2018 verstorben - der Parkinson hat sie besiegt....
>
> Unsere gemeinsamen Arbeiten nach dem Tod von Hans werden mir immer in wunderbarer Erinnerung bleiben . Fantastische Hilfe von Ralf und dir.....
>
> Hans, Joruun, Bärbel und nun Ralf...Wenigstens Du scheinst wieder in einer Beziehung zu leben. Wo lebst Du eigentlich? Ist Corona ein Thema? Gibt es Braak noch? Hast du das kleine Haus noch im Pusbackweg? Das Münstermann und das Merks Haus haben ja schön länger neue Besitzer.
>
> Liebe Grüße aus Hamburg und bleib schön gesund
>
> Kalöle

----
### Patric Schweizer - Patric.Schweizer@BlueWin.ch

> Hallo Andreas
>
> Ich danke Dir für Deine Nachricht, auch wenn es eine traurige Nachricht ist.
>
> Ich bin zur Zeit zu Hause wegen meines hohen Alters, aber mr geht es gesundheitlich gut.
>
> Gruss auch an die ganze Familie
>
> Patric

----
### Tim Voigt - Tim-Voigt@OutLook.com

> Wir sind alle sehr entsetzt. Möge Ralf vom Allmächtigen in Würde aufgenommen werden.
> Über den Schmezhaften Verlust möchten Wir der Familie von Ralf unser tiefstes Mitgefühl und Beileid zum Ausdruck bringen und wünschen von Herzen in der schweren Zeit sehr viel Kraft.
>
> Tim und Timea

----
### Axel Groll - AxelGroll@AOL.com

> Mein lieber Andreas,
>
> die Nachricht über Ralf's Ableben hat mich ganz schön getroffen und ich bin erschüttert.
> Ich erinnere mich noch gut an unsere letzte Begegnung genau vor drei Jahren im November.
>
> Am 25. November 2017 habe ich selbst auch meinen guten Freund Franco besser bekannt als "Pronto Salvatore" verloren,
> den ich eigentlich noch besuchen wollte, ich mich jedoch aus gegebenem Anlass entschieden hatte meinen Besuch bei Euch vorzuziehen.
> Daher werde ich mir Ralf's Todestag auch gut merken können.
>
> Ich habe mir die gemeinsamen Bilder mit Euch und Ralf angeschaut und dafür danke ich Dir.
> Es sind schöne Erinnerungen, die euch verbinden.
>
> Sein Gesundheitszustand soll nicht gut gewesen sein. Weiß man, woran Ralf gestorben ist?
>
> Richte bitte dem Sohn Marc, den ich nicht persönlich kenne bitte meine herzliche Anteilnahme.
>
> ...
>
> Liebe Grüße,
>
> Axel

----

### Sigrid - SiVo@Mehlbuedel.de

#### Am 29.11.2020 02:26, schrieb SiVo@Mehlbuedel.de:
> Auch mir war es vergönnt, Ralf kennenzulernen.
>
> Glücklicherweise bewahrheiteten sich seine "Unkereien" nicht. Denn danach passierte ihm immer 5 Jahre nach Mümmi dasselbe wie dem.
>
> Somit durfte er noch weitere 4,5 Jahre weiterhin auf dieser schönen Welt sein.
>
> Grad in letzter Zeit musste ich häufig an ihn denken. Es gibt wohl doch Dinge zwischen Himmel und Erde, die kein Mensch versteht.
>
> Von ganzem Herzen wünsche ich seinen Angehörigen und Freunden viel Kraft und Mut, mit dem Verlust klarzukommen!
>
> RIP Ralf, schlafe gut!
>
> Sigrid

----

#### Am 29.11.2020 10:57, ergänzte Andreas@Pannewitz.com:

> Hallo liebe Sigrid
>
> Wohl wohl.
>
> Apropos: Mümi's fünfter Todestag, der 12. Mai 2016.
>
> Ganz bewusst bin ich bereits am Sonntagmorgen des 8. Mai 2016 in Hamburg gelandet.
>
> ( Übrigens die einzige Reise seit 2013 ohne meine Frau.
>   Sie blieb in Burma, und zehn Wochen später stand ich
>   vor einem komplett neuen, unserem jetzigen Haus.)
>
> Ralf holte mich am Flughafen ab.
>
> Bei einen ersten Spaziergang an der Alster hörte ich erstmal lange zu...
>
> Und - Zufall? / Fügung? -
> Am 12 Mai hatte ich Ralf bei einer Ärztin, die ihm klar sagte:
> "*Heute* werden Sie *nicht* sterben."
>
> Nun - das war also abgewendet.
> Und auch -schon am Montag, dem 9. Mai- ein finanzieller Herzinfarkt.
>
> Indes: 'Alles Gut' war's damit noch lange lange nicht. Im Gegentum.
> Gesundheit? Bewusstsein dafür kann sich entwickeln, muss es aber nicht.
> Und obendrein: wenige Tage danach ein völlig dummer und überflüssiger Unfall...
>
> Und das ist schon wieder eine ganz andere Geschichte ...
>
> Viele Grüße - Auf ein Wiedersehen - Nächsten Sommer? Bleib bitte gesund.
>
> Andreas

----

### [Marc](MerkerMachine@gmx.de)

#### Am 27.11.2020 01:36, schrieb [Marc](MerkerMachine@gmx.de)

> Lieber Andreas liebe Win Win Maw,
>
> Vielen Dank für euren Mitleid und für die Fotos. Ich habe fast keine Fotos von ihm. Das hilft mir, lieben Gruß Marc

----
#### Am 27.11.2020 11:58, schrieb Andreas@Pannewitz.com an [Marc](MerkerMachine@gmx.de)

> So sehr gern lächelte und lachte er ...
>
> Drei Beispiele ...
>
> <img src="Img_2844-001.jpg" alt="Img_2844-001.jpg" style="zoom:50%;" /> <img src="_MG_4932-001.JPG" alt="_MG_4932-001.JPG" style="zoom: 56%;" /> <img src="_MG_4931-001.JPG" alt="_MG_4931-001.JPG" style="zoom:50%;" />

----

#### Am 27.11.2020 17:39, schrieb [Marc](MerkerMachine@gmx.de)

> Liebe Win Win Maw, lieber Andreas.
>
> du kannst mich immer anrufen. Melde dich einfach wenn es bei dir passt, zum Beispiel Samstag oder Sonntag.
>
> Am besten rufst du einmal an und wenn du mich nicht erreichst, rufst du 5 minuten später nochmal an. Meist bin ich ein paar Sekunden zu spät und dann ist schon die Mailbox dran.
>
> Ich muss heute noch in die Firma und schauen ob ich Heiratsurkunde und Scheidungspapiere finde. Die braucht der Bestatter.
>
> Danke übrignes nochmal für den Tipp mit dem Bestatter. Ich war da bereits im Kontakt mit einem Bestatter in Glinde (Weidemann), der sich jetzt kümmert.
>
> Die Beisetzung wird im Ewigforst (Sachsenwald) stattfinden, dort habe ich gestern einen schönen Baum für ihn ausgesucht.
>
> Danke für die Bilder, ich konnte die nicht runterladen, sondern nur abfotographieren. Jetzt habe ich sie nochmal in guter Qualität, das ist sehr schön.
>
> Besonders weil er so schön lacht und viel Freude mit euch hatte.
>
> Lieben Gruß
> Marc

----
#### Am 28.11.2020 11:52, schrieb Andreas@Pannewitz.com an [Marc](MerkerMachine@gmx.de)

##### 2. November 2010 - ca 21:30 - Alter Dorfkrug - am/beim Großensee - (ohne Peter)

> Moin moin lieber Marc
>
> Habe uns nun diese drei Portraits herausgearbeitet.
> Würden sogar in einen gemeinsamem Rahmen passen ...
>
> 2. November 2010 - im Alten Dorfkrug.
> https://www.google.de/maps/@53.6151807,10.3412116,21z
>
> Vorher hatten wir uns in Braak getroffen. Das erste Mal.
> Und per Handschlag (und vor Peter als Zeugen :-) )
> unsere gemeinsame Zukunft besiegelt.
>
> Nun waren wir alle etwas hungrig ...
> und es gab ja auch was zu begiessen.
>
> Lass uns dort gern gemeinsam mal 'schmausen',
> wenn wir (endlich!) wieder in Braak sind.
>
> Bis bald - am 'Rohr'
>
> Win Win Maw & Andreas
>
> <img src="IMG_3204-001.JPG" alt="IMG_3204-001.JPG" style="zoom:50%;" /> <img src="IMG_3207-001.JPG" alt="IMG_3207-001.JPG" style="zoom:50%;" /> <img src="IMG_3208-001.JPG" alt="IMG_3208-001.JPG" style="zoom:50%;" />

----
