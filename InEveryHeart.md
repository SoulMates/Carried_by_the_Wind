![Carried by the Wind](banner.jpg)

# In Every Heart

```
In every heart the spirit cries
but you must wait that is the prize
what we learned from fairy tales
in our childhood so it stays -
we don't know where and when?
Nobody knows - nobody knows
```

```
Every girl which you can find
has her own image on her mind
came from her childhood hidden dreams
which remains with her - so it seems
Even though you must wait very long
One day your dream will come along
```

```
because there’s love, because there’s love, in every heart
because there’s love, because there’s love, in every heart
because there’s love - a dream for everyone, for everyone, in every heart
```

```
Maybe you have to wait very long
but you can wait - you are so strong
and if it's really happen so
is the most important thing to know
you had your image on your mind
and it remains with you – forever
```

```
When you allow the spirit rise
see the emotions in your eyes
So live your dream and make it real
and do express the way you feel
we all have different believes -
there is a dream for everyone
```

```
because there’s love, because there’s love, in every heart
because there’s love, in every heart
because there’s love - a dream for everyone, for everyone, in every heart
```

## nur in der Langfassung:

### man spoken:
```
Who I am, I think I know
but who are you that brings that glow
Of life to me? I do not know
How did you made me love you so?
I'd like to think that you and me
could one day, becomes a "we"
To change from what I think we are
to someone else, is not that far,
On our own we can't achieve
this transformation can you believe?
```

### Guitar solo

```
How much time is given then to you
maybe the whole dream is coming true
or only some minutes from your life
for just the time it comes alive
- but he will be always in your heart
where ever you are, whatever you start
```

### Final Refrain: 2 times
```
because there’s love, because there’s love, in every heart
because there’s love, in every heart
because there’s love - a dream for everyone, for everyone, in every heart
```
---
![Carried by the Wind](avatar-large.jpg)
---
