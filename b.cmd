@Set pandoc=%Util%\Edit\Edit.md\PanDoc\pandoc.exe

@For %%f in (*.md) do @%pandoc% "%%f" -s --wrap=none -f gfm -t html -o "%%~nf.html"

@%pandoc% "RalfWilliFiedler.md" -s --toc --toc-depth=6 --wrap=none -f gfm -t html -o "RalfWilliFiedler.html"

@Set pandoc=
@Pause
@goto :EOF

usage: %0
using pandoc 
- all files *.md as input
- are parsed as gfm = Github Flavoured Markdown, and
- are output as stand-alone (-s) html
